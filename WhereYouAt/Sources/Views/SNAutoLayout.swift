//
//  SNAutoLayout.swift
//  WhereYouAt
//
//  Created by Narek Safaryan on 10/7/15.
//  Copyright © 2015 Narek Safaryan. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    enum SNAutoLayoutEdge{
        case Left
        case Right
        case Top
        case Bottom
    }
    
    /**
    Align edge to superview
    
    - parameter edge:      edge
    - parameter aConstant: constant value for offset from edge
    */
    func alignEdge(edge:SNAutoLayoutEdge, aConstant:CGFloat){
        self.alignEdge(edge, toItem: self.superview!, aConstant: aConstant)
    }
    
    func alignEdge(edge:SNAutoLayoutEdge, toItem:UIView, aConstant:CGFloat){
        switch edge {
        case SNAutoLayoutEdge.Left:
            self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem:toItem, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant:aConstant))
            break
        case SNAutoLayoutEdge.Right:
            self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem:toItem, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant:aConstant))
            break
        case SNAutoLayoutEdge.Top:
            self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem:toItem, attribute: NSLayoutAttribute.Top, multiplier: 1, constant:aConstant))
            break
        case SNAutoLayoutEdge.Bottom:
            self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem:toItem, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant:aConstant))
            break
        }
    }
    
    /**
    CenterX space to superview
    */
    func alignCenterX(aConstant:CGFloat){
        self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.superview, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant:aConstant))
    }
    
    /**
    CenterY space to superview
    */
    func alignCenterY(aConstant:CGFloat){
        self.superview!.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.superview, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant:aConstant))
    }
    
    func constrainWidth(aConstant:CGFloat){
        self.addConstraint(NSLayoutConstraint(item:self, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem:nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant:aConstant))
    }
    
    func constrainHeight(aConstant:CGFloat){
        self.addConstraint(NSLayoutConstraint(item:self, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem:nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant:aConstant))
    }
}

