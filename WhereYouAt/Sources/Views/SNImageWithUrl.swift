//
//  SNImageWithUrl.swift
//  WhereYouAt
//
//  Created by Narek Safaryan on 10/7/15.
//  Copyright © 2015 Narek Safaryan. All rights reserved.
//

import Foundation
import SDWebImage
import UIKit

extension UIImageView{
    public func sn_setImageWithURL(url: NSURL!, activityIndicatorStyle:UIActivityIndicatorViewStyle!){
        let loadingIndicator = self.viewWithTag(self.indicatorTag()) as! UIActivityIndicatorView
        loadingIndicator.removeFromSuperview()
        self.setupLoadingIndicator(activityIndicatorStyle).startAnimating()
        let completionBlock: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            self.stopLoadingAnimation()
        }
        self.sd_setImageWithURL(url, completed: completionBlock)
    }

    public func setupLoadingIndicator(activityIndicatorStyle:UIActivityIndicatorViewStyle!) -> UIActivityIndicatorView{
        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: activityIndicatorStyle)
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(loadingIndicator)
        loadingIndicator.alignCenterX(0)
        loadingIndicator.alignCenterY(0)
        loadingIndicator.constrainWidth(30)
        loadingIndicator.constrainHeight(30)
        loadingIndicator.tag = self.indicatorTag()
        return loadingIndicator
    }
    
    public func startLoadingAnimation(activityIndicatorStyle:UIActivityIndicatorViewStyle!){
        self.setupLoadingIndicator(activityIndicatorStyle).startAnimating()
    }
    
    public func stopLoadingAnimation(){
        let loadingIndicator:UIActivityIndicatorView = self.viewWithTag(self.indicatorTag()) as! UIActivityIndicatorView
        loadingIndicator.stopAnimating()
        loadingIndicator.removeFromSuperview()
    }
    
    public func indicatorTag() -> Int{
        return 9999
    }
}