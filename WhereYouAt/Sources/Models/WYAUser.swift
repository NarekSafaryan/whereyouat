//
//  WYAUser.swift
//  WhereYouAt
//
//  Created by Narek Safaryan on 10/6/15.
//  Copyright © 2015 Narek Safaryan. All rights reserved.
//

import Foundation
import ObjectMapper

class WYAUser: Mappable{
    var identifier:String = ""
    var username:String?
    var email_addresses:[AnyObject]?
    var first_name:String?
    var last_name:String?
    var last_update:[AnyObject]?
    var main_image_url:String?
    var user_description:String?
    var cover_image_url:String?
    var last_update_zulu:String?
    var last_update_utc:String?
    var last_update_standard:String?
    var last_update_ago:String?
    var last_update_seconds:Double?
    var current_user_can_modify:Bool?
    var current_user_can_content_edit:Bool?
    var display_name:String?
    var is_current_user:Bool?
    
    
    static let instance = WYAUser()
    
    init(){
        
    }
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        username         <- map["user_username"]
        identifier       <- map["_id"]
        email_addresses       <- map["user_email_addresses"]
        first_name              <- map["user_first_name"]
        last_name           <- map["user_last_name"]
        last_update            <- map["user_last_update"]
        main_image_url          <- map["user_main_image_url"]
        user_description           <- map["user_description"]
        cover_image_url              <- map["user_cover_image_url"]
        last_update_zulu              <- map["user_last_update_zulu"]
        last_update_utc      <- map["user_last_update_utc"]
        last_update_standard             <- map["user_last_update_standard"]
        last_update_ago     <- map["user_last_update_ago"]
        last_update_seconds           <- map["user_last_update_seconds"]
        current_user_can_modify          <- map["current_user_can_modify"]
        current_user_can_content_edit <- map["current_user_can_content_edit"]
        display_name		 <- map["user_display_name"]
        is_current_user		 <- map["user_is_current_user"]
    }
}