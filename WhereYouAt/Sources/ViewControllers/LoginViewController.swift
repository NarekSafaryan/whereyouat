//
//  LoginViewController.swift
//  WhereYouAt
//
//  Created by Narek Safaryan on 10/9/15.
//  Copyright © 2015 Narek Safaryan. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import ObjectMapper

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var userNameTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    var loginButton:UIButton = UIButton()
    
    var userData:NSData?
    var user:WYAUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.grayColor()
        self.userNameTextField.translatesAutoresizingMaskIntoConstraints = false
        self.userNameTextField.returnKeyType = UIReturnKeyType.Next
        self.userNameTextField.autocorrectionType = UITextAutocorrectionType.No
        self.userNameTextField.delegate = self;
        
        self.passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        self.passwordTextField.autocorrectionType = UITextAutocorrectionType.No
        self.passwordTextField.secureTextEntry = true
        self.passwordTextField.returnKeyType = UIReturnKeyType.Done
        self.passwordTextField.delegate = self;
        
        self.loginButton.translatesAutoresizingMaskIntoConstraints = false
        
        self.navigationItem.title = NSLocalizedString("WhereYouAt", comment: "Application name")
        
        self.view.addSubview(self.userNameTextField)
        self.userNameTextField.borderStyle = UITextBorderStyle.RoundedRect
        self.userNameTextField.placeholder = NSLocalizedString("Username", comment: "Username field palceholder")
        self.userNameTextField.textAlignment = NSTextAlignment.Center
        self.userNameTextField.alignEdge(.Left, aConstant: 30)
        self.userNameTextField.alignEdge(.Right, aConstant: -30)
        
        self.view.addSubview(self.passwordTextField)
        self.passwordTextField.borderStyle = UITextBorderStyle.RoundedRect
        self.passwordTextField.placeholder = NSLocalizedString("Password", comment: "Password field palceholder")
        self.passwordTextField.textAlignment = NSTextAlignment.Center
        self.passwordTextField.alignEdge(.Left, aConstant: 30)
        self.passwordTextField.alignEdge(.Right, aConstant: -30)
        self.passwordTextField.alignCenterY(0)
        
        self.view.addSubview(self.loginButton)
        self.loginButton.backgroundColor = UIColor.clearColor()
        self.loginButton.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        self.loginButton.setTitle(NSLocalizedString("Login", comment: "Login button title"), forState: UIControlState.Normal)
        self.loginButton.alignEdge(.Left, aConstant: 30)
        self.loginButton.alignEdge(.Right, aConstant: -30)
        self.loginButton.addTarget(self, action: Selector("didPressedLogin:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        var loginViewsBindingsDict = [String: AnyObject]()
        loginViewsBindingsDict["userNameTextField"] = self.userNameTextField
        loginViewsBindingsDict["passwordTextField"] = self.passwordTextField
        loginViewsBindingsDict["loginButton"] = self.loginButton
        let topAndBottomViewsSpaceConstraint = NSLayoutConstraint.constraintsWithVisualFormat("V:[userNameTextField]-10-[passwordTextField]-30-[loginButton]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: loginViewsBindingsDict)
        self.view.addConstraints(topAndBottomViewsSpaceConstraint)
    }
    
    func didPressedLogin(sender:UIButton)
    {
        self.view.endEditing(true)
        let username: String = self.userNameTextField.text!
        let password: String = self.passwordTextField.text!
      
        if username.isEmpty == true {
            let alert = UIAlertController(title: "Error!", message: "Please input username", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"), style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        if password.isEmpty == true {
            let alert = UIAlertController(title: "Error!", message: "Please input password", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK button"), style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        self.view.showLoading(UIActivityIndicatorViewStyle.WhiteLarge)
        
        
        
        let WYA: WhereYouAt = WhereYouAt()
        let params = ["user_username" : username, "user_password" : password]

        WYA.User.login(params, callback: { (response) -> Void in
            let
            success = response["success"] as! Bool
            dispatch_async(dispatch_get_main_queue()) {
                if (success){
                    let userTest: NSDictionary? = response["user"] as! NSDictionary!
                    if userTest != nil {
                        
                        // knowing it will cast as a dictionary, we cast it again as a traversable dictionary (the optional type is not traversable)
                        let user: NSDictionary = response["user"] as! NSDictionary!
                        let userMapper = Mapper<WYAUser>()
                        userMapper.map(response["user"], toObject: WYAUser.instance)
                        self.user = WYAUser.instance
                        let mainVC = MainViewController()
                        self.navigationController?.pushViewController(mainVC, animated: true)
                        self.view.hideLoading()

                        //
                        // this is where the front-end magic happens
                        //
                        // commands such as println("user=\(user)") will help see what is going on
                        //
                        // below is the 100% safest way to handle the optional parameters
                        for (key, value) in user {
                            // this is the absolute safest way to check for a parameter
                            if key as! String == "user_display_name" {
                                print(value as! String)
                            }
                            
                        }
                        
                    }
                } else {
                    let message = WYA.User.Result["message"] as? String
                    if message == nil {
                        _ = WYA.User.Error
                    }
                    let alertView = UIAlertView()
                    alertView.addButtonWithTitle("OK")
                    alertView.title = "WARNING"
                    alertView.message = message
                    alertView.show()
                    self.view.hideLoading()
                    return
                }
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.userNameTextField {
            self.passwordTextField.becomeFirstResponder()
            return false
        }
        else if textField == self.passwordTextField {
            self.didPressedLogin(self.loginButton)
            textField.resignFirstResponder()
            return true
        }
        return true
    }
}

