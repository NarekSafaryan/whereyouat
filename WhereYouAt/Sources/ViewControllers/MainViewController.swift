//
//  MainViewController.swift
//  WhereYouAt
//
//  Created by Narek Safaryan on 10/6/15.
//  Copyright © 2015 Narek Safaryan. All rights reserved.
//

import UIKit
import SDWebImage


class MainViewController: UIViewController {
    
    var profileImageView:UIImageView = UIImageView()
    var coverImageView:UIImageView = UIImageView()
    var topView:UIView = UIView()
    var bottomView:UIView = UIView()
    var aboutContainer:UIView = UIView()
    var nameLabel:UILabel = UILabel()
    var aboutTextView:UITextView = UITextView()
    var userData:NSData?
    var user:WYAUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.redColor()
        self.profileImageView.translatesAutoresizingMaskIntoConstraints = false
        self.topView.translatesAutoresizingMaskIntoConstraints = false
        self.bottomView.translatesAutoresizingMaskIntoConstraints = false
        
        self.topView.backgroundColor = UIColor.blackColor()
        self.view.addSubview(topView)
        self.topView.alignEdge(.Left, aConstant: 0)
        self.topView.alignEdge(.Right, aConstant: 0)
        self.topView.alignEdge(.Top, aConstant: 0)
        self.topView.constrainHeight(200)
        
        self.bottomView.backgroundColor = UIColor.greenColor()
        self.view.addSubview(bottomView)
        self.bottomView.alignEdge(.Left, aConstant: 0)
        self.bottomView.alignEdge(.Right, aConstant: 0)
        self.bottomView.alignEdge(.Bottom, aConstant: 0)
        
        /// Vertical space between topView and bottomView
        var topViewBindingsDict = [String: AnyObject]()
        topViewBindingsDict["topView"] = self.topView
        topViewBindingsDict["bottomView"] = self.bottomView
        let topAndBottomViewsSpaceConstraint = NSLayoutConstraint.constraintsWithVisualFormat("V:[topView]-0-[bottomView]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: topViewBindingsDict)
        self.view.addConstraints(topAndBottomViewsSpaceConstraint)
        
        self.coverImageView.translatesAutoresizingMaskIntoConstraints = false
        self.coverImageView.contentMode = .ScaleAspectFill
        self.topView.addSubview(self.coverImageView)
        
        /**
        Match cover to parent
        */
        self.coverImageView.alignEdge(.Left, aConstant: 0)
        self.coverImageView.alignEdge(.Right, aConstant: 0)
        self.coverImageView.alignEdge(.Bottom, aConstant: 0)
        self.coverImageView.alignEdge(.Top, aConstant: 0)

        /**
        Or you can do with visual formmat language like this
        
        var coverImageViewBindingsDict = [String: AnyObject]()
        coverImageViewBindingsDict["coverImageView"] = self.coverImageView
        
        let coverVConstraint = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[coverImageView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: coverImageViewBindingsDict)
        let coverHConstraint = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[coverImageView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: coverImageViewBindingsDict)
        self.view.addConstraints(coverVConstraint)
        self.view.addConstraints(coverHConstraint)

        */
        
        
        self.profileImageView.layer.cornerRadius = 75
        self.profileImageView.layer.masksToBounds = true
        self.profileImageView.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(profileImageView)
        self.profileImageView.constrainWidth(150)
        self.profileImageView.constrainHeight(150)
        self.profileImageView.alignCenterX(0)
        self.profileImageView.alignEdge(.Top, aConstant: 110)
        self.profileImageView.contentMode = .ScaleAspectFill
        
        self.aboutContainer.translatesAutoresizingMaskIntoConstraints = false
        self.bottomView.addSubview(aboutContainer)
        self.aboutContainer.alignEdge(.Left, aConstant: 10)
        self.aboutContainer.alignEdge(.Right, aConstant:-10)
        self.aboutContainer.alignEdge(.Bottom, aConstant:-10)
        self.aboutContainer.alignEdge(.Top, aConstant: 70)
        self.aboutContainer.layer.cornerRadius = 2
        self.aboutContainer.layer.masksToBounds = true
        self.aboutContainer.layer.borderColor = UIColor.blackColor().CGColor
        self.aboutContainer.layer.borderWidth = 3

        self.aboutContainer.backgroundColor = UIColor.whiteColor()
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.aboutTextView.translatesAutoresizingMaskIntoConstraints = false
        self.aboutContainer.addSubview(self.nameLabel)
        self.aboutContainer.addSubview(self.aboutTextView)
        
        self.nameLabel.alignEdge(.Top, aConstant: 15)
        self.nameLabel.font = UIFont.systemFontOfSize(30)
        
        self.nameLabel.alignCenterX(0)
        
        self.aboutTextView.font = UIFont.systemFontOfSize(20)
        self.aboutTextView.alignEdge(.Left, aConstant: 10)
        self.aboutTextView.alignEdge(.Right, aConstant:-10)
        self.aboutTextView.alignEdge(.Bottom, aConstant:-10)
        self.aboutTextView.alignEdge(.Top, aConstant: 50)
        self.aboutTextView.backgroundColor = UIColor.clearColor()
        
        self.profileImageView.startLoadingAnimation(.Gray)
        self.coverImageView.startLoadingAnimation(.WhiteLarge)
        
        self.user = WYAUser.instance
        if let _ = self.user?.cover_image_url {
            
        }
        self.navigationItem.title = self.user?.display_name
        self.nameLabel.text = self.user?.display_name
        self.aboutTextView.text = self.user?.user_description
        self.profileImageView.sn_setImageWithURL(NSURL(string: self.user!.main_image_url!), activityIndicatorStyle: .Gray)
        self.coverImageView.sn_setImageWithURL(NSURL(string: self.user!.cover_image_url!), activityIndicatorStyle: .Gray)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
